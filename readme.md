#UCP Stylesheets

Those are the CSS stylesheet for the UCP html export. Those can be used in wax and vivliostyle.


## Page master (used in editoria)

Vivliostyle works with the idea that pagemasters define the layout for the page. Therefore, we need a page master for specific pages. Those will handle the location of headers, the beginning of the chapter and so on.

What can be done is to set the master based on a list of predefined names in the bookbuilder: subvention, serie, and so on. It would be easy to set the name the 

| Class | Names | wax | vivliostyle | notes |
| --- | --- | --- | --- | --- |
| **Front matter** | | wax | vivliostyle | 
| `[data-type="subvention"]` | Subvention | | |
| `[data-type="serie"]` | Serie page | | | can be backmatter | 
| `[data-type="half-title"]` | Half Title | | |
| `[data-type="titlepage"]` | Title | | |
| `[data-type="copyright"]` | Copyright | | |
| `[data-type="dedication"]` | Dedication | | |
| `[data-type="epigraph"]` | Epigraph | | |
| `[data-type="fmBody"]` | Table of content | (Set as undefined front matter, as it use the same layout) | |
| `[data-type="fmBody"]` | Table of illustrations | | |
| `[data-type="fmBody"]` | table of tables | | |
| `[data-type="fmBody"]` | Front matter opening page (first page of any undefined front matter. Page number is set at the bottom and there is no running heads)| | |
| `[data-type="fmBody"]` | Front matter body page | | |
| **Body matter** | | wax | vivliostyle |
| `[data-type="part"]`  | Part opening page | | |
| `[data-type="part"]`  | Part content page | | |
| `[data-type="chapter"]`  | Chapter Opening (first page of the chapter. Page number is set at the bottom and there is no running heads) | | |
| `[data-type="chapter"]`  | Chapter body | | |
| **Back matter**| | wax | vivliostyle |
| `[data-type="bmBody"]`  | Back matter Opening (first page of any undefined backmatter. Page number is set at the bottom and there is no running heads) | | |
| `[data-type="bmBody"]`  | Back matter body | | |
| `[data-type="colophon"]`  | Colophon | | |




## Styles from the UCP printing specifications

To reduce the number of needed styles, we use the page master to define the look and feel of the differents elements. Here is a list of what is set in vivliostyle and how to access it in Editoria-Wax.

Also, would it be possible to set the style list based on the type of page? For example, if you're in the 'Serie page', you have access to the ST, SHN,SED and SL. All other styles being irrelevant in that.


| Class | Names | wax | vivliostyle |  notes
| --- | --- | --- | --- | --- |
| **FRONTMATTER** | |   wax | vivliostyle | |
| SV | Subvention  | General Text |  | 
| HT | Half Title  | *automated* | *automated* | This page can be automatically created at the export?
| ST | Series Title  | Title | `h1.ct` | 
| SHN | Series Head Note  | General Text | `p` | 
| SED | Series Editor  | | | 
| SL|  Series List  | Numbered list | `ol` | 
| TI | Title  | Title | `h1.ct` | 
| STI | Subtitle  | Subtitle | `p.edby`| 
| TORN | Title Ornament  | *automated* | *automated* | 
| EDBY | Edited by  | | `p.edby` | 
| AU | Author/Editor | | `p.au`|   
| AUX | Additional Contributor(s)  | | | 
| UCP | Logo  | *automated* | *automated* | 
| PUB | Publisher  | *automated* | *automated* | 
| PUBO | Publisher’s Offices  |*automated*  | *automated* | 
| CP | Copyright Page  |  | | 
| CIP | *will be happy to have Erich intel on that one*   |  | | 
| DED | Dedication  | General text | `[data-type="dedication"]  p` | 
| BEP | Book Prose Epigraph  | Epigraph: prose |`.sep` | 
| BEPSN | BEP Source Note  | Source note |`.exsn` | 
| BEPO | Book Poetry Epigraph  | Epigraph: poetry| `.sepo` | 
| BEPOSN | BEPO Source Note  | Source note | `.exsn`| 
| FMH | Frontmatter Head  | Title  | `.fmh` | 
| FMAU | Frontmatter Author  | | | 
| FM1 | Frontmatter 1 Head  | | | 
| FMHN | Frontmatter Headnote  | | | 
| FMTXT | Frontmatter Text  | | | 
| FMSIG | Frontmatter Signature  | | | 
| FIL | Frontmatter Illustration List  | | | 
| FAB | Frontmatter Abbreviation List  | | | 
| Table  of Contents  | | *automated* | *automated*  | 
| C-1 | FM/BM Listings  | *automated*  | *automated*  | 
| C-2 | PN/PT Listings  |  *automated*  | *automated*  | 
| C-3 | CN/CT Listings  | *automated*  | *automated*  | 
| C-4 | Chap. Subheads  |  *automated*  | *automated*  | 
| C-5 | Page Numbers  |  *automated*  | *automated*  | 


| **DISPLAY** | | wax | vivliostyle | notes 
| --- | --- | --- | --- | --- |
| PN | Part Number  | | |
| PT | Part Title with Rule  | | |
| PST | Part Subtitle  | | |
| PEP | Part Prose Epigraph  | | |
| PEPSN | PEP Source Note  | | |
| PEPO | Part Poetry Epigraph  | | |
| PEPOSN | PEPO Source Note  | | |
| PTXT | Part Opening Text  | | |
| CN | Chapter Number  | | |
| CT | Chapter Title with Rule  | Title | `h1.ct` |
| CST | Chapter Subtitle  | Subtitle | `h1.cst`|
| CAU | Chapter Author  | | `p.au` |
| CEP | Chapter Prose Epigraph  | | |
| CEPSN | CEP Source Note  | Source note | |
| CEPO | Chapter Poetry Epigraph  | | |
| CEPOSN | CEPO Source Note  | Source note | |
| Chapter | Opening Sink  | *automated* | |
| H1 | 1 Head  |  | |
| H2 | 2 Head  | | |
| H3 | 3 Head  | | |
| SEP | Subhead Prose Epigraph  | | |
| SEPSN | SEP Source Note  | | |
| SEPO | Subhead Poetry Epigraph  | | |
| SEPOSN | SEPO Source Note  | | |
|    || | |
| **TEXT** | |   wax | vivliostyle | notes | 
| GT | General Text  | General Text | `p` |
| RHL | / RHR Running Head  | *automated `.dup`* |  |
| FO | / DF Folio/Drop Folio  | *automated .folio* | *automated* |
| LS | Line Space  | |  |
| ORN | Ornament  | |  |
| LPTI | List or Poetry Title  | |  |
| NL | Numbered List  | Numbered List|  ol |
| UL | Unnumbered List  |Unnumbered List |  `ul.` |
| BL | Bulleted List  | Bulleted List  | ul |
| MCL | Multicolumn List  | |  |
| OL | Outline List  | |  |
| POE | Poetry (text-sized)  | |  |
| POSS | POE Stanza Space  | |  |
| DI | Dialogue (text-sized)  | |  |
| LT | Letter (text-sized)  | |  |
| LDA | LT Date  | |  |
| LSAL | LT Salutation  | |  |
| LSIG | LT Signature  | |  |
| EQ | Equation in Text  | |  |
| INTQ | Interview Question  | |  |
| INTA | Interview Answer  | |  |
| BOX | Boxed Text  | |  |
| BOXH | Boxed Text Header  | |  |
|    || | |
| **EXTRACTS**  | |   wax | vivliostyle | notes | 
| EXTI | Extract Title   | |  |
| EX | Prose Extract  | |  |
| EXSN | EX Source Note  | |  |
| EXEX | Prose Extract in EX  | |  |
| PXTI | Poetry Extract Title  | |  |
| PX | Poetry Extract  | |  |
| PXSS | PX Stanza Space  | |  |
| PXSN | PX Source Note  | |  |
| PXEX | Poetry Extract in EX  | |  |
| NLX | Numbered List Extract  | |  |
| ULX | Unnumbered List Extract  | |  |
| BLX | Bulleted List Extract  | |  |
| MCLX | Multicolumn List Extract  | |  |
| DX | Dialogue Extract  | |  |
| LX | Letter Extract  | |  |
| LXDA | LX Date  | |  |
| LXSAL | LX Salutation  | |  |
| LXSIG | LX Signature  | |  |
|    || | |
| **FOOTNOTES** | |   wax | vivliostyle | notes | 
| FN | Footnote  | |  |
| UFN | Unnumbered Footnote  | |  |
| FNEX | Footnote Prose Extract  | |  |
| FNEXSN | FNEX Source Note  | |  |
| FNPX | Footnote Poetry Extract  | |  |
| FNPXSN | FNPX Source Note  | |  |
| EN | Chapter Endnotes  | |  |
| UEN | Unnumbered Chapter Endnote  | |  |
| ENEX | Endnote Prose Extract  | |  |
| ENPX | Endnote Poetry Extract  | |  |
| ENNLX | EN Numbered List Extract  | |  |
| ENULX | EN Unnumbered List Extract  | |  |
| ENBLX | EN Bulleted List Extract  | |  |
| ENMCLX | EN Multicolumn List Extract  | |  |
|    || | |
| **TABLES** | |   wax | vivliostyle | notes | 
| TN | Table Number  | |  |
| TT | Table Title  | |  |
| TST | Table Subtitle  | |  |
| TSQB | Table Squib  | |  |
| T1 | Table 1 Head  | |  |
| T2 | Table 2 Head  | |  |
| TSPAN | Table Spanner Head  | |  |
| TCUT | Table Cut-in Head  | |  |
| TSTB | Table Stub  | |  |
| TB | Table Body  | |  |
| TSN | Table Source Note  | |  |
| TFN | Table Footnote  | |  |
||||
| **ILLUSTRATIONS** | |   wax | vivliostyle | notes | 
| FCAP | Figure Number & Caption  | |  |
| MCAP | Map Number & Caption  | |  |
| PCAP | Plate Number & Caption  | |  |
| | | |
| **BACKMATTER** | |   wax | vivliostyle | notes | 
| APN | Appendix Number  | |  |
| APT | Appendix Title  | |  |
| APST | Appendix Subtitle  | |  |
| BMH | Backmatter Head  | |  |
| BM1 | Backmatter 1 Head  | |  |
| BM2 | Backmatter 2 Head  | |  |
| BM3 | Backmatter 3 Head  | |  |
| BMHN | Backmatter Headnote  | |  |
| BMTXT | Backmatter Text  | |  |
| BMEX | Backmatter Prose Extract  | |  |
| BMEXSN | BMEX Source Note  | |  |
| BMPX | Backmatter Poetry Extract  | |  |
| BMPXSN | BMPX Source Note  | |  |
| BMNLX | BM Numbered List Extract  | |  |
| BMULX | BM Unnumbered List Extract  | |  |
| BMBLX | BM Bulleted List Extract  | |  |
| BMMCLX | BM Multicolumn List Extract  | |  |
| CHRON | Chronology  | |  |
| BAB | Backmatter Abbreviation List  | |  |
| BN | Backnotes  | |  |
| UBN | Unnumbered Backnote  | |  |
| GL | Glossary  | |  |
| BIB | Bibliography  | |  |
| CONTRIB | List of Contributors  | |  |
| BIL | Backmatter Illustration List  | |  |
| IN | Index  | |  |
| Colophon |  | |  |







  $('extract').each(replaceWithBlockquote('ex')) // delete when xsweet is updated
  $('extract-prose').each(replaceWithBlockquote('ex'))
  $('extract-poetry').each(replaceWithBlockquote('px'))
  $('epigraph-poetry').each(replaceWithBlockquote('sepo'))
  $('epigraph-prose').each(replaceWithBlockquote('sep'))
  $('bibliography-entry').each(replaceWithParagraph('bibliography-entry'))
  $('comment').each(replaceWithText)
  // $('chapter-number').each(replaceWithParagraph('sc-chapter-number'))
  $('chapter-title').each(replaceWithParagraph('ct'))
  $('chapter-subtitle').each(replaceWithParagraph('cst'))
  $('source-note').each(replaceWithParagraph('exsn'))
  $('ol[styling="qa"]').each(replaceWithList('di'))
  $('ol[styling="unstyled"]').each(replaceWithList('none'))