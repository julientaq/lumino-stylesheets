
### About content

- The white pages are not set in the book yet. I’m still trying to figure out the best way to automate that process. They will be added on future iterations of the book;
- Figures & tables are not yet styled correctly yet. As i try to find a way to keep the baseline rhythm as all cost, i need to spend some time on the formatting of those two elements before going further. The same thing goes for the figcaption;
- The content, based on the author docx files, is far from cleaned. I try to not spend too much time on thing that would only useful for that demo. Therefore, there may be not working links or weird content. On the same note, all em & en dashes should be cleaned instide editoria.




### About Kerning

On page 3, we have a larger white space between *D. Y.*. This happens because the type designer did kern the font for that specific suite of letters.
In Indesign, the only way to correct this is to manually correct the kerning where it’s needed.
One of the option we have using javascript is to use [kerning.js](http://kerningjs.com/) everytime we encounter a need of kerning. Also, this is tied to the typeface in use.

### About justification and hyphens

The javascript library used for the justification changed since the previous export, since browsers seem to handle hyphenization out of the box. We’ll see, as we’re making book if can get a better output. Also, we can tweak the white space to get a better feeling.

### About Running headers

The running headers are automatically detected inside the chapter. Depending on the lenght of the chapter/part, the last page may show the next running title. We’re looking at way to avoid this kind of error, but for now, it has to be checked manually.
<!-- option 1: one layout css per chapter with a different variable name for each running header -->
<!-- option 2: XLST scripts to seek and put the title at the right location inside the html file -->

Running header and page numbers are now aligned, as i figured out a way to get page number + running headers. Since the page number are old style figure, we may have a different feeling, but they are on the same baseline.

Also, as an experiment, i tried to have chapter title on the left pages and book title on the right ones.

### About notes

The end-notes were not designed in the previous version of the book. For now, they are as if it was a body chapter. It will change in the next iteration of the book.

#### Callouts

For now, note callout may appear at the beginning of the line. I have some idea to make it work, but it need more test. For now, we must do it by hand.
<!-- we can use the <nobr> tag which should start before the word and end after the note callout.-->

#### Notes numbering

The note are hardcoded in the previous html. Which means that renumbering everything by hand would take too much time for this demo. But, it’s easily doable as editoria is handling the note numbering directly. If it’s really needed i can spend some time to make it work, but it will take too long for the benefit it would bring.

### Misc fixes

- Added copyright, as an example. It may not be the good one though.
- Orisa was turned into Oriṣa. This issue come from the tools used to turn docx into html. With editoria and Xsweet running smoothly, this should not be happening again.
- p.31, there was an error from the docx file with errors in the numbering of titles.
